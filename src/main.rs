use git2::{DiffFormat, DiffLineType, Repository};
use lazy_static::lazy_static;
use regex::Regex;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let repo = Repository::open(".")?;

    let diff = repo.diff_tree_to_workdir_with_index(None, None)?;

    // FIXME remove FIXME
    lazy_static! {
        static ref TODO: Regex =
            Regex::new(r"([/#]+|<!--)\s*(TODO|FIXME|XXX|BUG|TOFIX):?\s*(.*)(-->)?").unwrap();
    }

    // TODO add
    // TODO:
    diff.print(DiffFormat::Patch, |_, _, line| {
        if line.origin_value() == DiffLineType::Addition {
            // BUG ?
            let linestr = std::str::from_utf8(line.content()).unwrap();
            if let Some(matches) = TODO.captures(linestr) {
                println!("\u{1b}[36m{}\u{1b}[m: {}", &matches[2], &matches[3]);
            }
        }
        true
    })?;

    Ok(())
}
